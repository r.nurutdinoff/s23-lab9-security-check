# Lab9 -- Security check

**Website:** www.npmjs.com

## Forgot password

    Cheatsheet: https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#forgot-password-cheat-sheet

Steps

1. own an account at www.npmjs.com

- checkout to https://www.npmjs.com/forgot enter the email or login alias for the exisitng account and hit 'Get Password Reset Link' button
- check the response body and time via Devtools

2. Dont own an account

- checkout to https://www.npmjs.com/forgot enter unexisted email or login
- check the response body and time via Devtools, check the mail for
  the letter

3. Make a comparison of data obtained of these 2 cases

| Test step                                | Result               |
| ---------------------------------------- | -------------------- |
| checkout to https://www.npmjs.com/forgot | Ok                   |
| enter valid email                        | Ok                   |
| Click "Get Password Reset Link"          | Ok                   |
| Check the response of Post request       | ![1](./images/1.PNG) |
| checkout to https://www.npmjs.com/forgot | Ok                   |
| Enter email non-existing email           | Ok                   |
| Click "Get Password Reset Link"          | Ok                   |
| Check the response of Post request       | ![1](./images/2.PNG) |

### Result

The reponse time and bodu of requests is the same. The npm doesnt respond with immediat response, it sends the recovery link internally. To conclude, npm has passed Forgot Password test

## Input validation

Cheatsheet: https://cheatsheetseries.owasp.org/cheatsheets/Input_Validation_Cheat_Sheet.html

Steps

1. checkout to https://www.npmjs.com/signup
2. make sure there is min and max limit of characters for password and login
3. make sure that empty email or email cant be submitted

| Test step                                 | Result                                          |
| ----------------------------------------- | ----------------------------------------------- |
| checkout to https://www.npmjs.com/signup  | Ok                                              |
| make sure there is min limit for password | ![1](./images/5.PNG)                            |
| make sure there is max limit for password | There is no message whet it hits 200 characters |
| make sure i cant submit with empty fields | ![1](./images/6.PNG)                            |
| make sure there is max limit for login    | ![1](./images/7.PNG)                            |
| cant enter invalid email                  | ![1](./images/8.PNG)                            |

### Result
     npm in most cases passed Input validation test

## User-ids test

    Cheatsheet: https://cheatsheetseries.owasp.org/cheatsheets/Authentication_Cheat_Sheet.html#user-ids

Steps

1. checkout to www.npmjs.com/login
2. Enter valid login in lowercase and regular password. Check that log in was successfull
3. Logout, checkout to www.npmjs.com/login
4. Enter valid login in uppercase and regular password. Check that log in was successfull

| Test step                                           | Result               |
| --------------------------------------------------- | -------------------- |
| checkout to www.npmjs.com/login                     | Ok                   |
| Enter valid login in lowercase and regular password | Ok                   |
| Click "Get Password Reset Link"                     | Ok                   |
| Check the response of Post request                  | ![1](./images/3.PNG) |
| checkout to https://www.npmjs.com/forgot            | Ok                   |
| Enter email non-existing email                      | Ok                   |
| Click "Get Password Reset Link"                     | Ok                   |
| Check the response of Post request                  | ![1](./images/4.PNG) |

### Result

npm has failed user-ids since it is not possible to login in uppercase
